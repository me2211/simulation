pragma solidity >= "0.8.20";

contract Vector {
    // private vector array
    int[] x;
    int[] y;
    int[] z;
    int[] dx;
    uint size;
    mapping(address => Vector) vectors;
    constructor (uint _size) {
        size = _size; // TODO - limit size of vectors by shaving off old data - difficult
        // a "running stack" datatype? 
        //counter that flags current index as loop
        // dx(_size)
        // count = 0;
        // count++
        // overwrite and implied indexing
    }
    // REVIEW - private diff would implicitly encrypt but may be trivial
    function append(int xS, int yS, int zS, int diff) public returns (uint) {
        // REVIEW - do I need to initialize vector? 
        x.push(xS);
        y.push(yS);
        z.push(zS);
        dx.push(diff);
        return dx.length;
    }
    /*
    // We need more data with the block.timestamp and the timestamp provided to get diff
    // move function can provide the block.timestamp and now the user can use both
    function depend(int xS, int yS, int zS, int tS, int tP) public returns (uint) { //bool?
        //return index;
        // TODO - loop through each array to find a match of diff (+) value equals tS
        for (uint i = 0; i < x.length; i++) {
            if (x[i] - diff[i] == );
        }
    }
    */
    function pretend(int xS, int yS, int zS, uint index) public returns (uint) {
        //return diff;
        require(x[index] == xS && y[index] == yS && z[index] == zS);
        return dx[index];
    }
    function key(int xS, int yS, int zS) public returns (uint) {
        x.push(xS);
        y.push(yS);
        z.push(zS);
        return z.length;
    }
    function place(uint index, int diff) returns (uint) {
        require(index > dx.length);
        dx.push(diff); // if placed with i, then we have fill in the blank look ahead issue
        return block.timestamp; // block.timestamp can be used as a second handshake
    }
}